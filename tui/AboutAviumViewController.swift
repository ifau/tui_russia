//
//  AboutAviumViewController.swift
//  hittelecom
//
//  Created by ifau on 05/07/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit

class AboutAviumViewController: UIViewController
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }

    @IBAction func webButtonPressed(sender: AnyObject)
    {
        UIApplication.shared.openURL(NSURL(string: "http://avium.ru")! as URL)
    }
    
    @IBAction func mailButtonPressed(sender: AnyObject)
    {
        UIApplication.shared.openURL(NSURL(string: "mailto:info@avium.ru")! as URL)
    }
    
    @IBAction func locationButtonPressed(sender: AnyObject)
    {
        UIApplication.shared.openURL(NSURL(string: "http://maps.apple.com/?ll=55.937982,37.862013&z=16")! as URL)
    }
    
    @IBAction func closeButtonAction(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
}
