//
//  OrderTourInteractor.swift
//  tui
//
//  Created by ifau on 15/10/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit

protocol OrderTourInteractorOutputProtocol: class
{
    func showMessage(_ message: String)
    func nameTextFieldText() -> String?
    func emailTextFieldText() -> String?
    func phoneTextFieldText() -> String?
}

class OrderTourInteractor
{
    weak var view: OrderTourInteractorOutputProtocol!
    
    var tour: ToursItemModel?
    
    fileprivate var isLoading = false
}

protocol OrderTourInteractorInputProtocol: class
{
    func imageUrlForCurrentTour() -> URL?
    func hotelInfoForCurrentTour() -> String
    func locationInfoForCurrentTour() -> String
    func dateInfoForForCurrentTour() -> String
    func mealInfoForForCurrentTour() -> String
    func priceInfoForForCurrentTour() -> String
    
    func orderButtonPressed()
}

extension OrderTourInteractor: OrderTourInteractorInputProtocol
{
    func imageUrlForCurrentTour() -> URL?
    {
        let width = Int(UIScreen.main.bounds.width * UIScreen.main.scale)
        let height = Int(width * 9 / 16)
        if let id = tour!.photos.first
        {
            return URL(string: "http://tui.ru/CmsPages/GetPhoto.aspx?width=\(width)&height=\(height)&fileguid=\(id)")
        }
        return nil
    }
    
    func hotelInfoForCurrentTour() -> String
    {
        guard tour != nil else { return "" }
        return "\(tour!.hotelName) \(tour!.hotelCategoryName)"
    }
    
    func locationInfoForCurrentTour() -> String
    {
        guard tour != nil else { return "" }
        return "\(tour!.countryName), \(tour!.cityName), \(tour!.districtName)"
    }
    
    func dateInfoForForCurrentTour() -> String
    {
        guard tour != nil else { return "" }
        return "\(tour!.departureDate) / \(tour!.nights) ночей"
    }
    
    func mealInfoForForCurrentTour() -> String
    {
        guard tour != nil else { return "" }
        switch tour!.pansionName
        {
            case "NO", "RO", "RR", "OB", "AO":
                return "Без питания (\(tour!.pansionName))"
            case "BB":
                return "Только завтрак (\(tour!.pansionName))"
            case "HB":
                return "Завтрак и ужин (\(tour!.pansionName))"
            case "FB":
                return "Завтрак, обед и ужин (\(tour!.pansionName))"
            case "ALL":
                return "Всё включено (\(tour!.pansionName))"
            default:
                return tour!.pansionName
        }
    }
    
    func priceInfoForForCurrentTour() -> String
    {
        guard tour != nil else { return "" }
        return "\(tour!.price) \(tour!.priceCurrency)"
    }
    
    func orderButtonPressed()
    {
        guard isLoading == false else { return }
        
        let name = view.nameTextFieldText()
        let email = view.emailTextFieldText()
        let phone = view.phoneTextFieldText()
        
        guard name != nil else { view.showMessage("Поле Имя не заполнено"); return }
        guard email != nil else { view.showMessage("Поле Email не заполнено"); return }
        guard phone != nil else { view.showMessage("Поле Телефон не заполнено"); return }
        
        isLoading = true
        ServiceAPI.sharedInstance.sendOrderTourRequest(name!, email: email!, phone: phone!, tourID: tour!.priceKey)
        { [unowned self] (success) in
            
            self.isLoading = false
            if success
            {
                self.view.showMessage("Ваша заявка успешно отправлена.")
            }
            else
            {
                self.view.showMessage("Произошла ошибка при отправке заявки. Попробуйте еще раз.")
            }
        }
    }
}
