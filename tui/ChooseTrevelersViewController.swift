//
//  ChooseTrevelersViewController.swift
//  tui
//
//  Created by ifau on 21/09/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit

class ChooseTrevelersViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet var tableView: UITableView!
    weak var interactor: FiltersTrevelersSelectorProtocol!
    
    var adultCount: Int!
    var childAges: [Int]!
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)
    {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.title = "Пассажиры"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Отменить", style: .plain, target: self, action: #selector(ChooseTrevelersViewController.dissmiss))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Выбрать", style: .done, target: self, action: #selector(ChooseTrevelersViewController.saveAndDissmiss))
        self.contentSizeInPopup = CGSize(width: 0, height: 240)
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad()
    {
        super.viewDidLoad()
        adultCount = interactor.getSelectedAdultCount()
        childAges = interactor.getSelectedChildAges()
        
        tableView.register(TrevelersTableViewCell.self, forCellReuseIdentifier: "Cell")
        tableView.delegate = self
        tableView.dataSource = self
    }

    func numberOfSections(in tableView: UITableView) -> Int
    {
        return childAges.count == 0 ? 1 : 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return section == 0 ? 2 : childAges.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TrevelersTableViewCell
        if indexPath.section == 0
        {
            cell.titleLabel.text = indexPath.row == 0 ? "Число взрослых" : "Число детей"
            cell.stepper.minimumValue = indexPath.row == 0 ? 1.0 : 0.0
            cell.stepper.maximumValue = indexPath.row == 0 ? 8.0 : 3.0
            cell.stepper.value = indexPath.row == 0 ? Double(adultCount) : Double(childAges.count)
        }
        else
        {
            cell.titleLabel.text = "Возраст ребёнка \(indexPath.row + 1)"
            cell.stepper.minimumValue = 2.0
            cell.stepper.maximumValue = 17.0
            cell.stepper.value = Double(childAges[indexPath.row])
        }
        cell.stepper.stepValue = 1
        cell.stepper.addTarget(self, action: #selector(ChooseTrevelersViewController.stepperDidChangedValue(_:)), for: .valueChanged)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 44.0
    }
    
    func stepperDidChangedValue(_ sender: ValueStepper)
    {
        let point = sender.convert(CGPoint.zero, to: tableView)
        let indexPath = tableView.indexPathForRow(at: point)
        guard indexPath != nil else { return }
        
        if indexPath!.section == 0 && indexPath!.row == 0
        {
                adultCount = Int(sender.value)
        }
        else if indexPath!.section == 0 && indexPath!.row == 1
        {
            if childAges.count < Int(sender.value)
            {
                childAges.append(2)
                childAges.count == 1
                    ? tableView.insertSections(IndexSet(integer: 1), with: .automatic)
                    : tableView.reloadSections(IndexSet(integer: 1), with: .automatic)
            }
            else
            {
                childAges.removeLast()
                childAges.count == 0
                    ? tableView.deleteSections(IndexSet(integer: 1), with: .automatic)
                    : tableView.reloadSections(IndexSet(integer: 1), with: .automatic)
            }
        }
        else if indexPath!.section == 1
        {
            childAges[indexPath!.row] = Int(sender.value)
        }
    }
    
    func dissmiss()
    {
        self.popupController!.dismiss()
    }
    
    func saveAndDissmiss()
    {
        interactor.didSelectTrevelersCount(adultCount, childAges: childAges)
        self.popupController!.dismiss()
    }
}

private class TrevelersTableViewCell: UITableViewCell
{
    var stepper: ValueStepper!
    var titleLabel: UILabel!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?)
    {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        
        stepper = ValueStepper()
        stepper.tintColor = UIColor(red: 240/255.0, green: 144/255.0, blue: 20/255.0, alpha: 1.0)
        stepper.backgroundColor = UIColor.clear
        stepper.autorepeat = true
        stepper.minimumValue = 0.0
        stepper.maximumValue = 20.0
        titleLabel = UILabel()
        titleLabel.font = UIFont(name: "TUITypeCyrillic-Regular", size: 18)!
        
        stepper.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        contentView.addSubview(stepper)
        contentView.addSubview(titleLabel)
        
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-8-[titleLabel]-8-[stepper(==141)]-8-|", options: .directionLeftToRight, metrics: nil, views: ["stepper": stepper, "titleLabel": titleLabel]))
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-8-[titleLabel]-8-|", options: .directionLeftToRight, metrics: nil, views: ["stepper": stepper, "titleLabel": titleLabel]))
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[stepper(==29)]", options: .directionLeftToRight, metrics: nil, views: ["stepper": stepper, "titleLabel": titleLabel]))
        contentView.addConstraint(NSLayoutConstraint(item: stepper, attribute: .centerY, relatedBy: .equal, toItem: contentView, attribute: .centerY, multiplier: 1.0, constant: 0.0))
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
}
