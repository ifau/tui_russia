//
//  OrderTourViewController.swift
//  tui
//
//  Created by ifau on 15/10/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit
import SHSPhoneComponent
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class OrderTourViewController: UIViewController
{
    var interactor: OrderTourInteractorInputProtocol!
    var router: OrderTourRouter!
    
    @IBOutlet var pictureImageView: UIImageView!
    @IBOutlet var hotelLabel: UILabel!
    @IBOutlet var locationLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var mealLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var phoneTextField: SHSPhoneTextField!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        initializeView()
    }
    
    func initializeView()
    {
        view.backgroundColor = lightBlueColor
        pictureImageView.kf.indicatorType = .activity
        pictureImageView.kf.setImage(with: interactor.imageUrlForCurrentTour())
        hotelLabel.text = interactor.hotelInfoForCurrentTour()
        locationLabel.text = interactor.locationInfoForCurrentTour()
        dateLabel.text = interactor.dateInfoForForCurrentTour()
        mealLabel.text = interactor.mealInfoForForCurrentTour()
        priceLabel.text = interactor.priceInfoForForCurrentTour()
        phoneTextField.formatter.setDefaultOutputPattern("+# (###) ###-##-##")
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(OrderTourViewController.hideKeyboard(_:)))
        view.addGestureRecognizer(tapGesture)
    }
    
    @IBAction func orderButtonPressed(_ sender: AnyObject)
    {
        interactor.orderButtonPressed()
    }
    
    func hideKeyboard(_ gesture: UITapGestureRecognizer)
    {
        view.endEditing(true)
    }
}

extension OrderTourViewController: UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
}

extension OrderTourViewController: OrderTourInteractorOutputProtocol
{
    func showMessage(_ message: String)
    {
        let alertController = UIAlertController(title: "TUI", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    func nameTextFieldText() -> String?
    {
        return nameTextField.text
    }
    
    func emailTextFieldText() -> String?
    {
        return emailTextField.text
    }
    
    func phoneTextFieldText() -> String?
    {
        return phoneTextField.text?.characters.count > 8 ? phoneTextField.text : nil
    }
}
