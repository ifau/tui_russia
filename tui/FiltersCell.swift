//
//  FiltersCell.swift
//  tui
//
//  Created by ifau on 21/09/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit

class FiltersCell: UITableViewCell
{
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    
    override func awakeFromNib()
    {
        backgroundColor = lightBlueColor
    }
}
