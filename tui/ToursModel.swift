//
//  ToursModel.swift
//  tui
//
//  Created by ifau on 08/10/16.
//  Copyright © 2016 avium. All rights reserved.
//

import Foundation
import Mapper

struct ToursModel: Mappable
{
    let items: [ToursItemModel]
    let totalItems: Int
    
    init(map: Mapper) throws
    {
        items = map.optionalFrom("SearchResult.SearchResultItems") ?? []
        totalItems = map.optionalFrom("SearchResult.TotalItems") ?? 0
    }
}

struct ToursItemModel: Mappable
{
    var hotelName: String
    var hotelCategoryName: String
    var countryName: String
    var cityName: String
    var districtName: String
    var departureDate: String
    var nights: Int
    var accommodationType: String
    var pansionName: String
    var price: Int
    var priceCurrency: String
    
    var photos: [String]
    var priceKey: String
    
    init(map: Mapper) throws
    {
        hotelName = map.optionalFrom("HotelName") ?? ""
        hotelCategoryName = map.optionalFrom("HotelCategoryName") ?? ""
        countryName = map.optionalFrom("CountryName") ?? ""
        cityName = map.optionalFrom("CityName") ?? ""
        districtName = map.optionalFrom("DistrictName") ?? ""
        departureDate = map.optionalFrom("DepartureDate") ?? ""
        nights = map.optionalFrom("Nights") ?? 0
        accommodationType = map.optionalFrom("AccommodationType") ?? ""
        pansionName = map.optionalFrom("NormalizedPansionName") ?? ""
        price = map.optionalFrom("Price") ?? 0
        priceCurrency = map.optionalFrom("PriceCurrency") ?? ""
        
        photos = map.optionalFrom("Photos") ?? []
        priceKey = map.optionalFrom("PriceKey") ?? ""
    }
}