//
//  AppDelegate.swift
//  tui
//
//  Created by ifau on 15/09/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

let blueColor = UIColor(red: 102/255.0, green: 167/255.0, blue: 202/255.0, alpha: 1.0)
let lightBlueColor = UIColor(red: 205/255.0, green: 224/255.0, blue: 234/255.0, alpha: 1.0)
let darkBlueColor = UIColor(red: 0, green: 52/255.0, blue: 82/255.0, alpha: 1.0)
let whiteColor = UIColor.white
let blackColor = UIColor.black

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        configureInitialViewController()
        configureAppearance()
        return true
    }
    
    func configureInitialViewController()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let navigationController = storyboard.instantiateViewController(withIdentifier: "InitialController") as! UINavigationController
        let filtersController = navigationController.topViewController as! FiltersViewController
        
        FiltersConfigurator.configure(filtersController)
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
    }
    
    func configureAppearance()
    {
        UINavigationBar.appearance().titleTextAttributes = [NSFontAttributeName: UIFont(name: "TUITypeCyrillicLight-Bold", size: 20)!, NSForegroundColorAttributeName : whiteColor]
        
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().tintColor = whiteColor
        UINavigationBar.appearance().backgroundColor = blueColor
        UINavigationBar.appearance().barTintColor = blueColor
        
//        UITableView.appearance().backgroundColor = lightOrangeColor
//        UITableViewCell.appearance().backgroundColor = lightOrangeColor
        
        UITableView.appearance().separatorStyle = .none
        UITableView.appearance().indicatorStyle = .white
        UITableView.appearance().separatorColor = UIColor.clear
        //UILabel.appearanceWhenContainedInInstancesOfClasses([UITableViewCell.self]).textColor = blackColor
        
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        // TUITypeCyrillicLight-Regular
        // TUITypeCyrillic-Bold
        // TUITypeCyrillic-Regular
        // TUITypeCyrillicLight-Bold
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

