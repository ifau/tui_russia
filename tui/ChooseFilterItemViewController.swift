//
//  ChooseFilterItemViewController.swift
//  tui
//
//  Created by ifau on 23/09/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit

class ChooseFilterItemViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet var tableView: UITableView!
    @IBOutlet var loadingIndicator: UIActivityIndicatorView!
    weak var interactor: FiltersItemsSelectorProtocol!
    
    var type: Int = 0
    var items: [FilterItemModel] = []
    var selectedIndexPaths: [IndexPath] = []
    var multipleSelectionEnabled: Bool = false
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)
    {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Отменить", style: .plain, target: self, action: #selector(ChooseFilterItemViewController.dissmiss))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Выбрать", style: .done, target: self, action: #selector(ChooseFilterItemViewController.saveAndDissmiss))
        self.contentSizeInPopup = CGSize(width: 0, height: 100)
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        if items.count == 0
        {
            tableView.isHidden = true
            loadingIndicator.startAnimating()
            self.navigationItem.rightBarButtonItem?.isEnabled = false
        }
        else
        {
            loadingIndicator.stopAnimating()
            loadingIndicator.isHidden = true
            self.contentSizeInPopup = CGSize(width: 0, height: 260)
            self.navigationItem.rightBarButtonItem?.isEnabled = true
        }
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func dataLoaded()
    {
        loadingIndicator.stopAnimating()
        loadingIndicator.isHidden = true
        tableView.isHidden = false
        self.contentSizeInPopup = CGSize(width: 0, height: 260)
        selectedIndexPaths = interactor.getSelectedIndexPaths(forType: type) as [IndexPath]
        tableView.reloadSections(IndexSet(integer: 0), with: .automatic)
        self.navigationItem.rightBarButtonItem?.isEnabled = true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.selectionStyle = .none
        cell.textLabel?.font = UIFont(name: "TUITypeCyrillic-Regular", size: 18)!
        cell.textLabel?.text = items[indexPath.row].text
        cell.tintColor = darkBlueColor
        cell.accessoryType = selectedIndexPaths.contains(indexPath) ? .checkmark : .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: false)
        guard items[indexPath.row].text.characters.count != 0 else { return }
        
        if multipleSelectionEnabled
        {
            if let index = selectedIndexPaths.index(of: indexPath)
            {
                selectedIndexPaths.remove(at: index)
            }
            else
            {
                selectedIndexPaths.append(indexPath)
            }
            tableView.reloadRows(at: [indexPath], with: .automatic)
        }
        else
        {
            if selectedIndexPaths.count == 0
            {
                selectedIndexPaths.append(indexPath)
                tableView.reloadRows(at: [indexPath], with: .automatic)
            }
            else if selectedIndexPaths[0] == indexPath
            {
                selectedIndexPaths.removeAll()
                tableView.reloadRows(at: [indexPath], with: .automatic)
            }
            else if selectedIndexPaths[0] != indexPath
            {
                let oldIndexPath = selectedIndexPaths[0]
                selectedIndexPaths.removeAll()
                selectedIndexPaths.append(indexPath)
                tableView.reloadRows(at: [oldIndexPath, indexPath], with: .automatic)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 44.0
    }
    
    func dissmiss()
    {
        self.popupController!.dismiss()
    }
    
    func saveAndDissmiss()
    {
        interactor.didSelectItemsAtIndexPaths(selectedIndexPaths, type: type)
        self.popupController!.dismiss()
    }
}
