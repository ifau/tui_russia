//
//  FiltersInteractor.swift
//  tui
//
//  Created by ifau on 21/09/16.
//  Copyright © 2016 avium. All rights reserved.
//

import Foundation
import STPopup

protocol FiltersInteractorOutputProtocol: class
{
    func reloadRowWithIndexPath(_ indexPath: IndexPath)
    func presentViewController(_ viewController: STPopupController)
    func showAlert(_ message: String)
    func navigateToNextScene()
}

class FiltersInteractor
{
    enum FiltersTypes: Int
    {
        case departureCity = 0, countries, dates, nightCount, trevelersCount, hotelCategory, meatCategory, price
    }

    fileprivate var data: FiltersModel?
    fileprivate var filterTitles: [String] = []
    fileprivate var filterDescriptions = ["", "", "", "", "", "", "", ""]
    fileprivate var prices = (1...10).map { $0 * 10000 } + stride(from: 15, through: 100, by: 5).map { $0 * 10000 }
    fileprivate var nights = (1...21).map { $0 }
    
    fileprivate weak var openedItemController: ChooseFilterItemViewController?
    fileprivate var selectedDepartureCity: [IndexPath] = []
    fileprivate var selectedCountries: [IndexPath] = []
    fileprivate var selectedHotelCategory: [IndexPath] = []
    fileprivate var selectedMeatCategory: [IndexPath] = []
    
    fileprivate var selectedDates: [Date] = []
    fileprivate var selectedAdultCount: Int = 1
    fileprivate var selectedChildAges: [Int] = []
    
    fileprivate var selectedNightsFromIndex: Int = 1
    fileprivate var selectedNightsToIndex: Int = 1
    fileprivate var selectedPriceIndex: Int = 1
    
    weak var view: FiltersInteractorOutputProtocol!
    
    init()
    {
        filterTitles.insert("Город отправления", at: FiltersTypes.departureCity.rawValue)
        filterTitles.insert("Страна отдыха*", at: FiltersTypes.countries.rawValue)
        filterTitles.insert("Даты вылета", at: FiltersTypes.dates.rawValue)
        filterTitles.insert("Количество ночей*", at: FiltersTypes.nightCount.rawValue)
        filterTitles.insert("Число пассажиров", at: FiltersTypes.trevelersCount.rawValue)
        filterTitles.insert("Категория отеля", at: FiltersTypes.hotelCategory.rawValue)
        filterTitles.insert("Категория питания", at: FiltersTypes.meatCategory.rawValue)
        filterTitles.insert("Цена", at: FiltersTypes.price.rawValue)
        loadFilters()
    }
    
    func loadFilters()
    {
        ServiceAPI.sharedInstance.getFilters
        { [unowned self] (success, filters) in
            
            if success
            {
                self.data = filters
            }
        }
    }
    
    func deliverItems()
    {
        if let data = data, let vc = openedItemController, let type = FiltersTypes(rawValue: vc.type)
        {
            switch type
            {
                case .departureCity:
                    vc.items = data.departureCities
                    vc.dataLoaded()
                case .countries:
                    vc.items = data.countries
                    vc.dataLoaded()
                case .hotelCategory:
                    vc.items = data.hotelCategories
                    vc.dataLoaded()
                case .meatCategory:
                    vc.items = data.mealTypes
                    vc.dataLoaded()
                default:
                    break
            }
        }
    }
    
    var request: String
    {
        get
        {
            var parameters: String = ""
            
            if selectedDepartureCity.count > 0
            {
                parameters += "&DepartureCity=\(data!.departureCities[selectedDepartureCity.first!.row].id)"
            }
            
            if selectedCountries.count > 0
            {
                parameters += "&Country=\(data!.countries[selectedCountries.first!.row].id)"
            }
            
            if selectedDates.count > 0
            {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                parameters += selectedDates.map{ dateFormatter.string(from: $0) }.map{ "&DepartureDate=\($0)|\($0)" }.reduce("", { $0 + $1 })
            }
            
            if selectedHotelCategory.count > 0
            {
                parameters += "&HotelCategory=\(data!.hotelCategories[selectedHotelCategory.first!.row].id)"
            }
            
            if selectedMeatCategory.count > 0
            {
                parameters += "&MealType=\(data!.mealTypes[selectedMeatCategory.first!.row].id)"
            }
            
            if selectedChildAges.count > 0
            {
                parameters += selectedChildAges.map{ "&ChildAges=\($0)" }.reduce("", { $0 + $1 })
            }
            
            if filterDescriptions[FiltersTypes.trevelersCount.rawValue].characters.count > 0
            {
                parameters += "&AdultCount=\(selectedAdultCount)"
            }
            
            if filterDescriptions[FiltersTypes.nightCount.rawValue].characters.count > 0
            {
                parameters += "&NightsFrom=\(nights[selectedNightsFromIndex] - 1)"
                parameters += "&NightsTo=\(nights[selectedNightsToIndex] - 1)"
            }
            
            if filterDescriptions[FiltersTypes.price.rawValue].characters.count > 0
            {
                parameters += "&PriceLimit=\(prices[selectedPriceIndex - 1])"
            }
            
            return parameters
        }
    }
}

// MARK: - Protocols

// MARK: Updating Root List

protocol FiltersListProtocol
{
    func numberOfFilters() -> Int
    func titleForFilterAtIndexPath(_ indexPath: IndexPath) -> String
    func descriptionForFilterAtIndexPath(_ indexPath: IndexPath) -> String
    func didSelectRowAtIndexPath(_ indexPath: IndexPath)
    func searchButtonDidPressed()
    func toursRequest() -> String
}

extension FiltersInteractor: FiltersListProtocol
{
    func numberOfFilters() -> Int
    {
        return filterTitles.count
    }
    
    func titleForFilterAtIndexPath(_ indexPath: IndexPath) -> String
    {
        return filterTitles[indexPath.row]
    }
    
    func descriptionForFilterAtIndexPath(_ indexPath: IndexPath) -> String
    {
        return filterDescriptions[indexPath.row]
    }
    
    func didSelectRowAtIndexPath(_ indexPath: IndexPath)
    {
        if let type = FiltersTypes(rawValue: indexPath.row)
        {
            let vc: UIViewController?
            switch type
            {
                case .departureCity:
                    
                    vc = ChooseFilterItemViewController(nibName: "ChooseFilterItemViewController", bundle: nil)
                    (vc as! ChooseFilterItemViewController).type = type.rawValue
                    (vc as! ChooseFilterItemViewController).interactor = self
                    (vc as! ChooseFilterItemViewController).selectedIndexPaths = selectedDepartureCity
                    if data != nil { (vc as! ChooseFilterItemViewController).items = data!.departureCities }
                    
                case .countries:
                    
                    vc = ChooseFilterItemViewController(nibName: "ChooseFilterItemViewController", bundle: nil)
                    (vc as! ChooseFilterItemViewController).type = type.rawValue
                    (vc as! ChooseFilterItemViewController).interactor = self
                    (vc as! ChooseFilterItemViewController).selectedIndexPaths = selectedCountries
                    if data != nil { (vc as! ChooseFilterItemViewController).items = data!.countries }
                    
                case .hotelCategory:
                    
                    vc = ChooseFilterItemViewController(nibName: "ChooseFilterItemViewController", bundle: nil)
                    (vc as! ChooseFilterItemViewController).type = type.rawValue
                    (vc as! ChooseFilterItemViewController).interactor = self
                    (vc as! ChooseFilterItemViewController).selectedIndexPaths = selectedHotelCategory
                    if data != nil { (vc as! ChooseFilterItemViewController).items = data!.hotelCategories }
                    
                case .meatCategory:
                    
                    vc = ChooseFilterItemViewController(nibName: "ChooseFilterItemViewController", bundle: nil)
                    (vc as! ChooseFilterItemViewController).type = type.rawValue
                    (vc as! ChooseFilterItemViewController).interactor = self
                    (vc as! ChooseFilterItemViewController).selectedIndexPaths = selectedMeatCategory
                    if data != nil { (vc as! ChooseFilterItemViewController).items = data!.mealTypes }
                    
                case .dates:
                    
                    vc = ChooseDateViewController(nibName: "ChooseDateViewController", bundle: nil)
                    (vc as! ChooseDateViewController).interactor = self
                    
                case .trevelersCount:
                    
                    vc = ChooseTrevelersViewController(nibName: "ChooseTrevelersViewController", bundle: nil)
                    (vc as! ChooseTrevelersViewController).interactor = self
                
                case .nightCount:
                    
                    vc = ChooseRangeViewController(nibName: "ChooseRangeViewController", bundle: nil)
                    (vc as! ChooseRangeViewController).interactor = self
                    (vc as! ChooseRangeViewController).type = type.rawValue
                    (vc as! ChooseRangeViewController).fromItems = nights
                    (vc as! ChooseRangeViewController).toItems = nights
                
                case .price:
                    vc = ChooseRangeViewController(nibName: "ChooseRangeViewController", bundle: nil)
                    let formatter = NumberFormatter()
                    formatter.locale = Locale(identifier: "ru_RU")
                    formatter.maximumFractionDigits = 0
                    formatter.numberStyle = NumberFormatter.Style.currency
                    (vc as! ChooseRangeViewController).interactor = self
                    (vc as! ChooseRangeViewController).type = type.rawValue
                    (vc as! ChooseRangeViewController).toItems = prices
                    (vc as! ChooseRangeViewController).numberFormatter = formatter
            }
            vc!.title = filterTitles[type.rawValue]
            let popupVC = STPopupController(rootViewController: vc!)
            popupVC.style = .bottomSheet
            view.presentViewController(popupVC)
        }
    }
    
    func searchButtonDidPressed()
    {
        if !((selectedCountries.count > 0) && (filterDescriptions[FiltersTypes.nightCount.rawValue].characters.count > 0))
        {
            view.showAlert("Страна отдыха и количество ночей не заполнены")
        }
        else
        {
            view.navigateToNextScene()
        }
    }
    
    func toursRequest() -> String
    {
        return request
    }
}

// MARK: Select Filter Items

protocol FiltersItemsSelectorProtocol: class
{
    func getSelectedIndexPaths(forType type: Int) -> [IndexPath]
    func didSelectItemsAtIndexPaths(_ indexPaths: [IndexPath], type: Int)
}

extension FiltersInteractor: FiltersItemsSelectorProtocol
{
    func getSelectedIndexPaths(forType type: Int) -> [IndexPath]
    {
        switch FiltersTypes(rawValue: type)!
        {
            case .departureCity:
                return selectedDepartureCity
            case .countries:
                return selectedCountries
            case .hotelCategory:
                return selectedHotelCategory
            case .meatCategory:
                return selectedMeatCategory
            default:
                return []
        }
    }
    
    func didSelectItemsAtIndexPaths(_ indexPaths: [IndexPath], type: Int)
    {
        switch FiltersTypes(rawValue: type)!
        {
            case .departureCity:
                selectedDepartureCity = indexPaths
                let string = indexPaths.map { data!.departureCities[$0.row].text }.sorted().reduce("", { $0 == "" ? $1 : $0 + ", " + $1 })
                filterDescriptions[type] = string
                view.reloadRowWithIndexPath(IndexPath(row: type, section: 0))
            
            case .countries:
                selectedCountries = indexPaths
                let string = indexPaths.map { data!.countries[$0.row].text }.sorted().reduce("", { $0 == "" ? $1 : $0 + ", " + $1 })
                filterDescriptions[type] = string
                view.reloadRowWithIndexPath(IndexPath(row: type, section: 0))
            
            case .hotelCategory:
                selectedHotelCategory = indexPaths
                let string = indexPaths.map { data!.hotelCategories[$0.row].text }.sorted().reduce("", { $0 == "" ? $1 : $0 + ", " + $1 })
                filterDescriptions[type] = string
                view.reloadRowWithIndexPath(IndexPath(row: type, section: 0))
            
            case .meatCategory:
                selectedMeatCategory = indexPaths
                let string = indexPaths.map { data!.mealTypes[$0.row].text }.sorted().reduce("", { $0 == "" ? $1 : $0 + ", " + $1 })
                filterDescriptions[type] = string
                view.reloadRowWithIndexPath(IndexPath(row: type, section: 0))
            
            default:
                break
        }
    }
}

// MARK: Select Ranges

protocol FiltersRangesSelectorProtocol: class
{
    func getSelectedRange(forType type: Int) -> (Int, Int)
    func didSelectRange(_ fromIndex: Int, toIndex: Int, type: Int)
}

extension FiltersInteractor: FiltersRangesSelectorProtocol
{
    func getSelectedRange(forType type: Int) -> (Int, Int)
    {
        switch FiltersTypes(rawValue: type)!
        {
            case .nightCount:
                return (selectedNightsFromIndex, selectedNightsToIndex)
            case .price:
                return (1, selectedPriceIndex)
            default:
                return (1, 1)
        }
    }
    
    func didSelectRange(_ fromIndex: Int, toIndex: Int, type: Int)
    {
        switch FiltersTypes(rawValue: type)!
        {
            case .nightCount:
                selectedNightsFromIndex = fromIndex
                selectedNightsToIndex = toIndex
                let string = "От \(fromIndex) до \(toIndex)"
                filterDescriptions[type] = string
                view.reloadRowWithIndexPath(IndexPath(row: type, section: 0))
            
            case .price:
                selectedPriceIndex = toIndex
                let formatter = NumberFormatter()
                formatter.locale = Locale(identifier: "ru_RU")
                formatter.maximumFractionDigits = 0
                formatter.numberStyle = NumberFormatter.Style.currency
                let number = NSNumber(value: prices[toIndex - 1] as Int)
                let string = "До \(formatter.string(from: number)!)"
                filterDescriptions[type] = string
                view.reloadRowWithIndexPath(IndexPath(row: type, section: 0))
            default:
                break
        }
    }
}

// MARK: Select Date

protocol FiltersDateSelectorProtocol: class
{
    func getSelectedDates() -> [Date]
    func didSelectDates(_ dates: [Date])
}

extension FiltersInteractor: FiltersDateSelectorProtocol
{
    func getSelectedDates() -> [Date]
    {
        return selectedDates
    }
    
    func didSelectDates(_ dates: [Date])
    {
        selectedDates = dates.sorted { $0.compare($1) == ComparisonResult.orderedAscending }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM"
        let string = selectedDates.map{ dateFormatter.string(from: $0) }.reduce("", { $0 == "" ? $1 : $0 + ", " + $1 })
        let index = FiltersTypes.dates.rawValue
        filterDescriptions[index] = string
        view.reloadRowWithIndexPath(IndexPath(row: index, section: 0))
    }
}

// MARK: Select Trevelers

protocol FiltersTrevelersSelectorProtocol: class
{
    func getSelectedAdultCount() -> Int
    func getSelectedChildAges() -> [Int]
    func didSelectTrevelersCount(_ adultCount: Int, childAges: [Int])
}

extension FiltersInteractor: FiltersTrevelersSelectorProtocol
{
    func getSelectedAdultCount() -> Int
    {
        return selectedAdultCount
    }
    
    func getSelectedChildAges() -> [Int]
    {
        return selectedChildAges
    }
    
    func didSelectTrevelersCount(_ adultCount: Int, childAges: [Int])
    {
        selectedAdultCount = adultCount
        selectedChildAges = childAges
        
        var string = "\(selectedAdultCount) \(selectedAdultCount == 1 ? "взрослый" : "взрослых")"
        if selectedChildAges.count > 0
        {
            string += " и \(selectedChildAges.count) \(selectedChildAges.count == 1 ? "ребенок" : "детей")"
        }
        let index = FiltersTypes.trevelersCount.rawValue
        filterDescriptions[index] = string
        view.reloadRowWithIndexPath(IndexPath(row: index, section: 0))
    }
}
