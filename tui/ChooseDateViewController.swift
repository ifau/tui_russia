//
//  ChooseDateViewController.swift
//  tui
//
//  Created by ifau on 21/09/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit
import FSCalendar

class ChooseDateViewController: UIViewController, FSCalendarDelegate
{
    @IBOutlet var calendar: FSCalendar!
    weak var interactor: FiltersDateSelectorProtocol!
    var selectedDates: [Date]!
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)
    {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.title = "Даты вылета"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Отменить", style: .plain, target: self, action: #selector(ChooseDateViewController.dissmiss))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Выбрать", style: .done, target: self, action: #selector(ChooseDateViewController.saveAndDissmiss))
        self.contentSizeInPopup = CGSize(width: 0, height: 260)
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        calendar.appearance.headerDateFormat = "MMMM YYYY"
        calendar.appearance.selectionColor = darkBlueColor
        calendar.appearance.headerTitleColor = blueColor
        calendar.appearance.weekdayTextColor = blueColor
        
        calendar.delegate = self
        calendar.allowsMultipleSelection = true
        
        selectedDates = interactor.getSelectedDates() as [Date]!
        for date in selectedDates { calendar.select(date) }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date) -> Bool
    {
        if date.compare(Date()) == ComparisonResult.orderedDescending // date after current date
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date)
    {
        selectedDates.append(date)
    }
    
    func calendar(_ calendar: FSCalendar, didDeselect date: Date)
    {
        let dates = selectedDates.filter { $0 != date }
        selectedDates = dates
    }
    
    func dissmiss()
    {
        self.popupController!.dismiss()
    }
    
    func saveAndDissmiss()
    {
        interactor.didSelectDates(selectedDates)
        self.popupController!.dismiss()
    }
}
