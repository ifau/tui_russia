//
//  FiltersViewController.swift
//  tui
//
//  Created by ifau on 16/09/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit
import STPopup

class FiltersViewController: UIViewController
{
    @IBOutlet var tableView: UITableView!

    var interactor: FiltersListProtocol!
    var router: FiltersRouter!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        tableView.backgroundColor = lightBlueColor
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 50
    }
    
    @IBAction func searchButtonPressed(_ sender: AnyObject)
    {
        interactor.searchButtonDidPressed()
    }
}

extension FiltersViewController: UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return interactor.numberOfFilters()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! FiltersCell
        
        cell.titleLabel.text = interactor.titleForFilterAtIndexPath(indexPath)
        cell.descriptionLabel.text = interactor.descriptionForFilterAtIndexPath(indexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: false)
        interactor.didSelectRowAtIndexPath(indexPath)
    }
}

extension FiltersViewController: FiltersInteractorOutputProtocol
{
    func reloadRowWithIndexPath(_ indexPath: IndexPath)
    {
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
    
    func presentViewController(_ viewController: STPopupController)
    {
        viewController.present(in: self)
    }
    
    func navigateToNextScene()
    {
        router.navigateToSearchTours()
    }
    
    func showAlert(_ message: String)
    {
        let alertController = UIAlertController(title: "TUI", message: message, preferredStyle: .alert)
        let closeAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alertController.addAction(closeAction)
        present(alertController, animated: true, completion: nil)
    }
}
