//
//  FiltersConrigurator.swift
//  tui
//
//  Created by ifau on 05/10/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit

class FiltersConfigurator
{
    static func configure(_ viewController: FiltersViewController)
    {
        let interactor = FiltersInteractor()
        interactor.view = viewController
        
        let router = FiltersRouter()
        router.viewController = viewController
        
        viewController.interactor = interactor
        viewController.router = router
    }
}

extension FiltersViewController
{
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        router.passDataToNextScene(segue)
    }
}
