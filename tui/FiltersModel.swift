//
//  FiltersModel.swift
//  tui
//
//  Created by ifau on 18/09/16.
//  Copyright © 2016 avium. All rights reserved.
//

import Foundation
import Mapper

struct FiltersModel: Mappable
{
    let departureCities: [FilterItemModel]
    let countries: [FilterItemModel]
    let hotelCategories: [FilterItemModel]
    let mealTypes: [FilterItemModel]
    
    init(map: Mapper) throws
    {
        departureCities = map.optionalFrom("SearchFilter.Filters.DepartureCity.FilterItems") ?? []
        countries = map.optionalFrom("SearchFilter.Filters.Country.FilterItems") ?? []
        hotelCategories = map.optionalFrom("SearchFilter.Filters.HotelCategories.FilterItems") ?? []
        mealTypes = map.optionalFrom("SearchFilter.Filters.MealTypes.FilterItems") ?? []
    }
}

struct FilterItemModel: Mappable
{
    var id: String
    var text: String
    
    init(map: Mapper) throws
    {
        id = map.optionalFrom("Id") ?? ""
        text = map.optionalFrom("Text") ?? ""
    }
}