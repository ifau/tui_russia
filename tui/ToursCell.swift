//
//  ToursCell.swift
//  tui
//
//  Created by ifau on 05/10/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit

class ToursCell: UITableViewCell
{
    @IBOutlet var pictureImageView: UIImageView!
    @IBOutlet var hotelLabel: UILabel!
    @IBOutlet var locationLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var mealLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    
    override func awakeFromNib()
    {
        backgroundColor = lightBlueColor
    }
}
