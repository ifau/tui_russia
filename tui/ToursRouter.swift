//
//  ToursRouter.swift
//  tui
//
//  Created by ifau on 08/10/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit

protocol ToursRouterInputProtocol
{
    func navigateToOrderTour()
}

class ToursRouter: ToursRouterInputProtocol
{
    weak var viewController: ToursViewController!
    
    // MARK: Navigation
    
    func navigateToOrderTour()
    {
        viewController.performSegue(withIdentifier: "ShowOrderTourScene", sender: nil)
    }
    
    // MARK: Communication
    
    func passDataToNextScene(_ segue: UIStoryboardSegue)
    {
        if segue.identifier == "ShowOrderTourScene"
        {
            passDataToOrderTourScene(segue)
        }
    }
    
    func passDataToOrderTourScene(_ segue: UIStoryboardSegue)
    {
        let orderTourViewController = segue.destination as! OrderTourViewController
        let tour = viewController.interactor.selectedTour()
        OrderTourConfigurator.configure(orderTourViewController, tour: tour)
    }
}
