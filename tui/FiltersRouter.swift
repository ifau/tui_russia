//
//  FiltersRouter.swift
//  tui
//
//  Created by ifau on 05/10/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit

protocol FiltersRouterInputProtocol
{
    func navigateToSearchTours()
}

class FiltersRouter: FiltersRouterInputProtocol
{
    weak var viewController: FiltersViewController!
    
    // MARK: Navigation
    
    func navigateToSearchTours()
    {
        viewController.performSegue(withIdentifier: "ShowSearchToursScene", sender: nil)
    }
    
    // MARK: Communication
    
    func passDataToNextScene(_ segue: UIStoryboardSegue)
    {
        if segue.identifier == "ShowSearchToursScene"
        {
            passDataToSearchToursScene(segue)
        }
    }
    
    func passDataToSearchToursScene(_ segue: UIStoryboardSegue)
    {
        let toursViewController = segue.destination as! ToursViewController
        let toursRequest = viewController.interactor.toursRequest()
        ToursConfigurator.configure(toursViewController, request: toursRequest)
    }
}
