//
//  ChooseRangeViewController.swift
//  tui
//
//  Created by ifau on 26/09/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit

class ChooseRangeViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate
{
    @IBOutlet var pickerView: UIPickerView!
    weak var interactor: FiltersRangesSelectorProtocol!
    
    var type: Int = 0
    var numberFormatter: NumberFormatter?
    var toItems: [Int] = []
    var fromItems: [Int]?
    
    var selectedFromRow: Int = 1
    var selectedToRow: Int = 1
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)
    {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Отменить", style: .plain, target: self, action: #selector(ChooseRangeViewController.dissmiss))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Выбрать", style: .done, target: self, action: #selector(ChooseRangeViewController.saveAndDissmiss))
        self.contentSizeInPopup = CGSize(width: 0, height: 200)
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        pickerView.dataSource = self
        pickerView.delegate = self
        
        let range = interactor.getSelectedRange(forType: type)
        selectedFromRow = range.0
        selectedToRow = range.1
        
        if fromItems == nil
        {
            pickerView.selectRow(selectedToRow, inComponent: 0, animated: false)
        }
        else
        {
            pickerView.selectRow(selectedFromRow, inComponent: 0, animated: false)
            pickerView.selectRow(selectedToRow, inComponent: 1, animated: false)
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        var rowsCount: Int
        if fromItems != nil
        {
            rowsCount = component == 0 ? fromItems!.count : toItems.count
        }
        else
        {
            rowsCount = toItems.count
        }
        return rowsCount + 1
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return fromItems != nil ? 2 : 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        if row == 0
        {
            if fromItems != nil
            {
                return component == 0 ? "От" : "До"
            }
            else
            {
                return "До"
            }
        }
        else if fromItems != nil
        {
            return component == 0 ? "\(fromItems![row - 1])" : "\(toItems[row - 1])"
        }
        else
        {
            return numberFormatter != nil ? numberFormatter!.string(from: NSNumber(value: toItems[row - 1] as Int)) : "\(toItems[row - 1])"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        guard row > 0 else
        {
            let lastRow = (component == 0 && fromItems != nil) ? selectedFromRow : selectedToRow
            pickerView.selectRow(lastRow, inComponent: component, animated: true)
            return
        }
        
        guard fromItems != nil else
        {
            selectedToRow = row
            return
        }
        
        if component == 0
        {
            if fromItems![row - 1] > toItems[selectedToRow - 1]
            {
                pickerView.selectRow(selectedFromRow, inComponent: component, animated: true)
            }
            else
            {
                selectedFromRow = row
            }
        }
        else if component == 1
        {
            if toItems[row - 1] < fromItems![selectedFromRow - 1]
            {
                pickerView.selectRow(selectedToRow, inComponent: component, animated: true)
            }
            else
            {
                selectedToRow = row
            }
        }
    }
    
    func dissmiss()
    {
        self.popupController!.dismiss()
    }
    
    func saveAndDissmiss()
    {
        interactor.didSelectRange(selectedFromRow, toIndex: selectedToRow, type: type)
        self.popupController!.dismiss()
    }
}
