//
//  ToursConfigurator.swift
//  tui
//
//  Created by ifau on 08/10/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit

class ToursConfigurator
{
    static func configure(_ viewController: ToursViewController, request: String)
    {
        let interactor = ToursInteractor()
        interactor.view = viewController
        interactor.request = request
        
        let router = ToursRouter()
        router.viewController = viewController
        
        viewController.interactor = interactor
        viewController.router = router
    }
}

extension ToursViewController
{
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        router.passDataToNextScene(segue)
    }
}
