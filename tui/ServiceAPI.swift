//
//  ServiceAPI.swift
//  tui
//
//  Created by ifau on 28/09/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit

class ServiceAPI: NSObject
{
    static let sharedInstance = ServiceAPI()
    fileprivate var requestsCount : Int = 0
    {
        didSet
        {
            UIApplication.shared.isNetworkActivityIndicatorVisible = requestsCount > 0 ? true : false
        }
    }
    
    fileprivate func sendGetRequest(_ urlString: String, completion: @escaping (_ data: Data?, _ response: URLResponse?, _ error: Error?) -> ())
    {
        let session = URLSession.shared
        let encodedUrlString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        
        let url = URL(string: encodedUrlString)!
        var request = URLRequest(url: url)
        
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        requestsCount = requestsCount + 1
        
        let task = session.dataTask(with: request, completionHandler: { [unowned self] (data: Data?, response: URLResponse?, error: Error?) -> Void in
            
            self.requestsCount = self.requestsCount - 1
            DispatchQueue.main.async(execute: { completion(data, response, error) })
        }) 
        task.resume()
    }
    
    fileprivate func sendPostRequest(_ urlString: String, dataString: String, completion: @escaping (_ data: Data?, _ response: URLResponse?, _ error: Error?) -> ())
    {
        let session = URLSession.shared
        
        let encodedUrlString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        
        let url = URL(string: encodedUrlString)!
        var request = URLRequest(url: url)
        
        request.httpMethod = "POST"
        request.httpBody = dataString.data(using: String.Encoding.utf8)!
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        requestsCount = requestsCount + 1
        
        let task = session.dataTask(with: request, completionHandler: { [unowned self] (data: Data?, response: URLResponse?, error: Error?) -> Void in
            
            self.requestsCount = self.requestsCount - 1
            DispatchQueue.main.async(execute: { completion(data, response, error) })
        }) 
        task.resume()
    }
    
    // MARK: - API Actions
    
    func getFilters(_ completion: @escaping (_ success: Bool, _ filters: FiltersModel?) -> ())
    {
        getFilters(retryCount: 10) { (filters) in
            
            completion(filters != nil, filters)
        }
    }
    
    fileprivate func getFilters(retryCount retryCounter: Int, completion: @escaping (_ filters: FiltersModel?) -> ())
    {
        if retryCounter == 0
        {
            completion(nil)
        }
        let counter = retryCounter - 1
        
        let url = "http://vt-api.tui.ru:8962/api-agency/search/Get?format=json"
        sendGetRequest(url)
        { [unowned self] (data: Data?, response: URLResponse?, error: Error?) -> () in
            
            if error == nil
            {
                do
                {
                    let JSON = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                    let filters = FiltersModel.from(JSON)
                    filters != nil ? completion(filters) : self.getFilters(retryCount: counter, completion: completion)
                }
                catch
                {
                    self.getFilters(retryCount: counter, completion: completion)
                }
            }
            else
            {
                self.getFilters(retryCount: counter, completion: completion)
            }
        }
    }
    
    func getTours(_ params: String, completion: @escaping (_ success: Bool, _ tours: ToursModel?) -> ())
    {
        getTours(retryCount: 10, params: params, completion: { (tours) in
            
            completion(tours != nil, tours)
        })
    }
    
    fileprivate func getTours(retryCount retryCounter: Int, params: String, completion: @escaping (_ tours: ToursModel?) -> ())
    {
        if retryCounter == 0
        {
            completion(nil)
        }
        let counter = retryCounter - 1
        
        let url = "http://vt-api.tui.ru:8962/api-agency/search/GetFacetsAndTours?format=json&GroupBy=WebSearch&currency=RUB\(params)"
        sendGetRequest(url)
        { [unowned self] (data: Data?, response: URLResponse?, error: Error?) -> () in
            
            if error == nil
            {
                do
                {
                    let JSON = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                    let tours = ToursModel.from(JSON)
                    tours != nil ? completion(tours) : self.getTours(retryCount: counter, params: params, completion: completion)
                }
                catch
                {
                    self.getTours(retryCount: counter, params: params, completion: completion)
                }
            }
            else
            {
                self.getTours(retryCount: counter, params: params, completion: completion)
            }
        }
    }
    
    func sendOrderTourRequest(_ name: String, email: String, phone:String, tourID: String, completion: @escaping (_ success: Bool) -> ())
    {
        var params = Dictionary <String, String>()
        params["To"] = "tuiapp@yandex.ru"
        params["Subject"] = "Заявка из мобильного приложения TUI Россия"
        params["Body"] = "Имя: \(name)\nEmail: \(email)\nНомер телефона: \(phone)\nID путёвки: \(tourID)"
        
        let data = try! JSONSerialization.data(withJSONObject: params, options: [])
        let json = String(data: data, encoding: String.Encoding.utf8)!
        let url = "http://aviumemailsender.azurewebsites.net/api/Sender"
        
        sendPostRequest(url, dataString: json) { (data: Data?, response: URLResponse?, error: Error?) -> () in
            
            if error == nil
            {
                do
                {
                    let JSON = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                    let success = JSON["Item"] as! Bool
                    completion(success)
                }
                catch
                {
                    completion(false)
                }
            }
            else
            {
                completion(false)
            }
        }
        
        // second time
        
        var params2 = Dictionary <String, String>()
        params2["To"] = "tuischuk@yandex.ru"
        params2["Subject"] = "Заявка из мобильного приложения TUI Россия"
        params2["Body"] = "Имя: \(name)\nEmail: \(email)\nНомер телефона: \(phone)\nID путёвки: \(tourID)"
        
        let data2 = try! JSONSerialization.data(withJSONObject: params2, options: [])
        let json2 = String(data: data2, encoding: String.Encoding.utf8)!
        let url2 = "http://aviumemailsender.azurewebsites.net/api/Sender"
        
        sendPostRequest(url2, dataString: json2) { (data: Data?, response: URLResponse?, error: Error?) -> () in }
    }
}
