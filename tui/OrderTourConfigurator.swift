//
//  OrderTourConfigurator.swift
//  tui
//
//  Created by ifau on 15/10/16.
//  Copyright © 2016 avium. All rights reserved.
//

import Foundation

class OrderTourConfigurator
{
    static func configure(_ viewController: OrderTourViewController, tour: ToursItemModel)
    {
        let interactor = OrderTourInteractor()
        interactor.view = viewController
        interactor.tour = tour
        
        let router = OrderTourRouter()
        router.viewController = viewController
        
        viewController.interactor = interactor
        viewController.router = router
    }
}
