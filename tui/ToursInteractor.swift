//
//  ToursInteractor.swift
//  tui
//
//  Created by ifau on 05/10/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit

protocol ToursInteractorOutputProtocol: class
{
    func showLoadingIndicator()
    func hideLoadingIndicator()
    func insertRows(_ indexPaths: [IndexPath])
    func setNavigationBarTitle(_ title: String)
    func goToPreviousViewWithMessage(_ message: String)
    func goToNextView()
}

class ToursInteractor
{
    weak var view: ToursInteractorOutputProtocol!
    
    var request: String?
    
    fileprivate var currentPage = 1
    fileprivate let pageSize = 40
    fileprivate var isLoading = false
    fileprivate var tours: [ToursItemModel] = []
    fileprivate var totalToursCount = 0
    
    fileprivate var selectedIndexPath: IndexPath?
    
    func loadData()
    {
        if request != nil
        {
            let params = "\(request!)&page=\(currentPage)&pageSize=\(pageSize)"
            
            isLoading = true
            ServiceAPI.sharedInstance.getTours(params, completion:
            { [unowned self] (success, tours) in
                
                self.isLoading = false
                if success
                {
                    if tours!.items.count > 0
                    {
                        var indexPaths: [IndexPath] = []
                        for item in tours!.items
                        {
                            self.tours.append(item)
                            let index = self.tours.count - 1
                            indexPaths.append(IndexPath(row: index, section: 0))
                        }
                        self.totalToursCount = tours!.totalItems
                        self.currentPage += 1

                        self.view.insertRows(indexPaths)
                        self.view.setNavigationBarTitle("Найдено \(tours!.totalItems) туров")
                    }
                    else if self.currentPage == 1
                    {
                        self.view.goToPreviousViewWithMessage("По заданным критериям не найдено ни одного тура")
                    }
                }
            })
        }
    }
}

protocol ToursInteractorInputProtocol: class
{
    func viewDidLoad()
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    func itemsCount() -> Int
    func imageUrlForItemAtIndex(_ index: Int) -> URL?
    func hotelInfoForItemAtIndex(_ index: Int) -> String
    func locationInfoForItemAtIndex(_ index: Int) -> String
    func dateInfoForItemAtIndex(_ index: Int) -> String
    func mealInfoForItemAtIndex(_ index: Int) -> String
    func priceInfoForItemAtIndex(_ index: Int) -> String
    
    func didSelectRowAtIndexPath(_ indexPath: IndexPath)
    func selectedTour() -> ToursItemModel
}

extension ToursInteractor: ToursInteractorInputProtocol
{
    func viewDidLoad()
    {
        view.setNavigationBarTitle("Поиск туров...")
        loadData()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        if !isLoading && (tours.count > 0) && (tours.count < totalToursCount)
        {
            let currentOffset = scrollView.contentOffset.y
            let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
            let deltaOffset = maximumOffset - currentOffset
            
            if deltaOffset <= 0
            {
                loadData()
            }
        }
    }
    
    func itemsCount() -> Int
    {
        return tours.count
    }
    
    func imageUrlForItemAtIndex(_ index: Int) -> URL?
    {
        let width = Int(UIScreen.main.bounds.width * UIScreen.main.scale)
        let height = Int(width * 9 / 16)
        if let id = tours[index].photos.first
        {
            return URL(string: "http://tui.ru/CmsPages/GetPhoto.aspx?width=\(width)&height=\(height)&fileguid=\(id)")
        }
        return nil
    }
    
    func hotelInfoForItemAtIndex(_ index: Int) -> String
    {
        let item = tours[index]
        return "\(item.hotelName) \(item.hotelCategoryName)"
    }
    
    func locationInfoForItemAtIndex(_ index: Int) -> String
    {
        let item = tours[index]
        return "\(item.countryName), \(item.cityName), \(item.districtName)"
    }
    
    func dateInfoForItemAtIndex(_ index: Int) -> String
    {
        let item = tours[index]
        return "\(item.departureDate) / \(item.nights) ночей"
    }
    
    func mealInfoForItemAtIndex(_ index: Int) -> String
    {
        let item = tours[index]
        switch item.pansionName
        {
            case "NO", "RO", "RR", "OB", "AO":
                return "Без питания (\(item.pansionName))"
            case "BB":
                return "Только завтрак (\(item.pansionName))"
            case "HB":
                return "Завтрак и ужин (\(item.pansionName))"
            case "FB":
                return "Завтрак, обед и ужин (\(item.pansionName))"
            case "ALL":
                return "Всё включено (\(item.pansionName))"
            default:
                return item.pansionName
        }
    }
    
    func priceInfoForItemAtIndex(_ index: Int) -> String
    {
        let item = tours[index]
        return "\(item.price) \(item.priceCurrency)"
    }
    
    func didSelectRowAtIndexPath(_ indexPath: IndexPath)
    {
        selectedIndexPath = indexPath
        view.goToNextView()
    }
    
    func selectedTour() -> ToursItemModel
    {
        return tours[selectedIndexPath!.row]
    }
}
