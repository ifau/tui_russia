//
//  ToursViewController.swift
//  tui
//
//  Created by ifau on 05/10/16.
//  Copyright © 2016 avium. All rights reserved.
//

import UIKit
import Kingfisher

class ToursViewController: UIViewController
{
    @IBOutlet var tableView: UITableView!
    
    var interactor: ToursInteractorInputProtocol!
    var router: ToursRouter!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        interactor.viewDidLoad()
        
        tableView.backgroundColor = lightBlueColor
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 200
    }
}

extension ToursViewController: UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return interactor.itemsCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ToursCell
        
        cell.pictureImageView.kf.indicatorType = .activity
        cell.pictureImageView.kf.setImage(with: interactor.imageUrlForItemAtIndex(indexPath.row))
        cell.hotelLabel.text = interactor.hotelInfoForItemAtIndex(indexPath.row)
        cell.locationLabel.text = interactor.locationInfoForItemAtIndex(indexPath.row)
        cell.dateLabel.text = interactor.dateInfoForItemAtIndex(indexPath.row)
        cell.mealLabel.text = interactor.mealInfoForItemAtIndex(indexPath.row)
        cell.priceLabel.text = interactor.priceInfoForItemAtIndex(indexPath.row)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        let _cell = cell as! ToursCell
        _cell.pictureImageView.kf.cancelDownloadTask()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: false)
        interactor.didSelectRowAtIndexPath(indexPath)
    }
}

extension ToursViewController: UIScrollViewDelegate
{
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        interactor.scrollViewDidScroll(scrollView)
    }
}

extension ToursViewController: ToursInteractorOutputProtocol
{
    func showLoadingIndicator()
    {
        
    }
    
    func hideLoadingIndicator()
    {
        
    }
    
    func insertRows(_ indexPaths: [IndexPath])
    {
        tableView.insertRows(at: indexPaths, with: .automatic)
    }
    
    func setNavigationBarTitle(_ title: String)
    {
        navigationItem.title = title
    }
    
    func goToPreviousViewWithMessage(_ message: String)
    {
        let alertController = UIAlertController(title: "TUI", message: message, preferredStyle: .alert)
        let closeAction = UIAlertAction(title: "OK", style: .cancel) { (UIAlertAction) in
            self.navigationController?.popViewController(animated: true)
        }
        alertController.addAction(closeAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func goToNextView()
    {
        router.navigateToOrderTour()
    }
}
